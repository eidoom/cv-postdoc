#!/usr/bin/env bash

BIB=cv.bib

bibtool -i ${BIB} -r bibtool.rsc -o ${BIB}
