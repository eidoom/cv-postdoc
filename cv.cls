\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cv}[2021-11-05 Ryan's custom CV class]

\LoadClass[10pt, a4paper]{article}

\usepackage[maxbibnames=99,sorting=ymnt]{biblatex}
\usepackage{emoji}
\usepackage{fontspec}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage{titlesec}
\usepackage{xcolor}

\addbibresource{cv.bib}

\DeclareSortingTemplate{ymnt}{
  \sort{
    \field{presort}
  }
  \sort[final]{
    \field{sortkey}
  }
  \sort[direction=descending]{
    \field{sortyear}
    \field{year}
    \literal{0000}
  }
  \sort[direction=descending]{
    \field[padside=left,padwidth=2,padchar=0]{month}
    \literal{00}
  }
  \sort{
    \field{sortname}
    \field{author}
    \field{editor}
    \field{translator}
  }
  \sort{
    \field{sorttitle}
    \field{title}
  }
}

% colours
\def\rulecolour{orange!90!black}
\def\linkcolour{blue!90!black}

% Headings format
\titleformat{\section}{\Large\raggedright\heavy}{}{0em}{}[\color{\rulecolour}{\titlerule[0.5pt]}]
\titlespacing{\section}{0pt}{3pt}{2ex}

% Link colour
\hypersetup{colorlinks,breaklinks,urlcolor=\linkcolour,linkcolor=\linkcolour,citecolor=\linkcolour}

% Custom table column formats
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}
\newcolumntype{Y}[0]{>{\centering\arraybackslash}X}
\newcolumntype{Z}[0]{>{\raggedright\arraybackslash}X}

% Custom fonts
\setmainfont[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Semibold.otf,
  ItalicFont = SourceSansPro-LightIt.otf,
  BoldItalicFont = SourceSansPro-SemiboldIt.otf
]{SourceSansPro-Light.otf}

\newfontfamily\heavy[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Bold.otf,
  ItalicFont = SourceSansPro-It.otf,
  BoldItalicFont = SourceSansPro-BoldIt.otf
]{SourceSansPro-Regular.otf}

% Suppress hyphenation
\hyphenpenalty=10000
