# [cv-postdoc](https://gitlab.com/eidoom/cv-postdoc)

Live here:
* [CV](http://eidoom.gitlab.io/cv-postdoc/cv.pdf)
* [Publications](http://eidoom.gitlab.io/cv-postdoc/pubs.pdf)
