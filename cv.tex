\documentclass{cv}

\usepackage[hmargin=20mm, vmargin=20mm]{geometry}

\begin{document}

\pagestyle{empty} % non-numbered pages

\begin{center}

    {\Huge \heavy Ryan Moodie}

    \vspace{4pt}

    {\small \href{mailto:ryaniain.moodie@unito.it}{ryaniain.moodie@unito.it}}

\end{center}

\section{Research}
\begin{tabularx}{\linewidth}{C{4em} c Z}
    Oct 2023 & \emoji{flag-italy} & {\heavy Postdoctoral researcher, Dipartimento di Fisica, Università degli Studi di Torino e INFN} \\
    | & & Two-loop many-scale QCD and QED amplitude computations, precision LHC phenomenology, \\
    May 2022 & & and neural networks for emulating amplitudes and solving Feynman integrals \\
    \\
    Apr 2022 & \emoji{flag-england} & {\heavy PhD, Institute of Particle Physics Phenomenology (IPPP), University of Durham} \\
    | & & Supervisor: Simon Badger \\
    Oct 2018 & & Thesis: Precision QCD corrections to gluon-initiated diphoton-plus-jet production at the LHC~\cite{Moodie:2022dxs}  \\
\end{tabularx}\\

\section{Education}
\begin{tabularx}{\linewidth}{C{4em} c Z}
    Jun 2018 & \emoji{flag-england} & {\heavy MASt in Applied Mathematics (Part III Maths), University of Cambridge}\\
    | & & Examined in \\
    Oct 2017\vfill & & {
        \begin{tabularx}{\linewidth}{L{19ex} L{19ex} X}
            General Relativity & Quantum Field Theory & Standard Model \\
            Statistical Field Theory & String Theory & Symmetries, Fields and Particles
        \end{tabularx}
    } \\
    \\
    Jun 2017 & \emoji{flag-scotland} & {\heavy MPhys (Hons) in Theoretical Physics, University of St Andrews}\\
    | & & Dissertation: \href{https://bitbucket.org/ryanmoodie/two-mode-cavity-dicke-model-report/raw/master/report.pdf}{Simulating continuous symmetry breaking with cold atoms in optical cavities} \\
    Sep 2013  & & Selected awards \\
    & & {
        \begin{tabularx}{\linewidth}{X}
            Theoretical Physics Project Prize for best final-year project \\
            Medal (Theoretical Physics Fifth Level) for highest graded student
        \end{tabularx}
    }
\end{tabularx}\\

\section{Teaching}
\begin{center}
    Associate Fellow of the Higher Education Academy (AFHEA)\\
    Durham Excellence in Learning and Teaching Award Level 1\\
\end{center}

\begin{tabularx}{\linewidth}{R{13ex} X}
    2020--21 & \href{https://www.dur.ac.uk/faculty.handbook/module_description/?year=2020\&module_code=PHYS4181}{Durham level 4 particle theory} workshop demonstrator \\
    2018--20, 21--22 & \href{https://www.dur.ac.uk/faculty.handbook/archive/module_description/?year=2019\&module_code=PHYS3621}{Durham level 3 quantum, nuclear, and particle physics} workshop demonstrator \\
\end{tabularx}\\

\section{Public engagement and outreach}
\begin{tabularx}{\linewidth}{R{13ex} Z}
    Durham & I was involved with the \href{https://www.dur.ac.uk/celebrate.science/}{Celebrate Science Festivals}, \href{https://www.dur.ac.uk/science.outreach/sciencefestival/}{Schools' Science Festivals}, \href{https://www.ogdentrust.com/university-links/school-physicist-of-the-year}{Schools' Physicist of the Year events}, \href{https://conference.ippp.dur.ac.uk/event/880/timetable/}{International Particle Physics Masterclasses}, and in various open days. I maintained the \href{https://github.com/eidoom/GaltonBoard}{software} for our \href{https://www.modellinginvisible.org/exhibits/}{outreach exhibits}. \\
    \href{https://www.st-andrews.ac.uk/physics-astronomy/about/outreach/}{St Andrews} & I helped run workshops at local schools, with the Science Discovery Days, and at various open days as Ambassador.
\end{tabularx}\\

\section{Equity, diversity, and inclusion (EDI)}
\begin{tabularx}{\linewidth}{R{13ex} Z}
    Durham & I was a member of \href{https://www.ippp.dur.ac.uk/equity-diversity-and-inclusivity/}{the Institute's EDI group}. We held seminars, promoted EDI, and strived to identify and address issues related to EDI within the Institute. \\
\end{tabularx}\\

\section{Programming experience}
\begin{tabularx}{\linewidth}{R{13ex} X}
    Primary & C++, Python \\
    Secondary & Bash, C, Haskell, HTML/CSS, JavaScript, Julia, LaTeX, Linux, Lisp, Mathematica, Rust, SQL \\
\end{tabularx}\\

\begin{tabularx}{\linewidth}{R{13ex} Z}
    Durham & I ran the \href{https://computing-club.notes.dmaitre.phyip3.dur.ac.uk/}{Physics Computing Club}, organising weekly departmental training sessions and seminars.\\
\end{tabularx}\\

\pagebreak
\section{Conferences, schools, seminars, and workshops}
\begin{tabularx}{\textwidth}{R{14pt} l c l Z r}
    2023 & Jun & \emoji{flag-switzerland} & Zurich [Virtual] & \href{https://indico.psi.ch/event/13708/contributions/43409/}{Radiative corrections and Monte Carlo tools for low-energy hadronic cross sections in $e^+e^-$ collisions} & \href{https://doi.org/10.5281/zenodo.8019969}{Presented} \\
    2022 & Nov & \emoji{flag-european-union} & CERN [V] & \href{https://indico.cern.ch/event/1203113/}{CMS Physics Generators meetings} & \href{https://doi.org/10.5281/zenodo.7760525}{Presented} \\
    & Oct & \emoji{flag-italy} & Bari & \href{https://indico.cern.ch/event/1106990/contributions/4997230/}{21st International Workshop on Advanced Computing and Analysis Techniques in Physics Research (ACAT) --- AI meets Reality} & \href{https://doi.org/10.5281/zenodo.7760420}{Presented}~\cite{Moodie:2023pql} \\
    & Sep & \emoji{flag-england} & Newcastle & \href{https://conference.ippp.dur.ac.uk/event/1100/contributions/5770/}{8th International Workshop on High Precision for Hard Processes (HP2)} & \href{https://doi.org/10.5281/zenodo.7759875}{Presented} \\
    & Aug & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/1104/contributions/5674/}{4th Workstop/Thinkstart: Towards N$^3$LO for $\gamma^*\to \ell\bar \ell$} & \href{https://doi.org/10.5281/zenodo.7759592}{Presented} \\
    & Jun & \emoji{flag-germany} & Würzburg & Würzburg particle theory seminars & \href{https://doi.org/10.5281/zenodo.7759561}{Presented} \\
    & Feb & \emoji{flag-england} & Cambridge & Cavendish HEP seminars & \href{https://doi.org/10.5281/zenodo.7763067}{Presented} \\
    & Jan & \emoji{flag-italy} & Turin [V] & \href{https://indico.cern.ch/event/1096550/}{Precision high multiplicity scattering at the LHC} & \\
    & & \emoji{flag-england} & Durham [V] & \href{https://conference.ippp.dur.ac.uk/event/1073/}{IPPP internal seminars} & \href{https://doi.org/10.5281/zenodo.7763103}{Presented} \\
    2021 & Dec & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/1065/}{Early-career Theory Forum (YTF) 2021} (committee member) & \\
    & & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/1062/}{Annual Theory Meeting (ATM) 2021} & \\
    & Nov & \emoji{flag-south-korea} & Daejeon [V] & \href{https://indico.cern.ch/event/855454/contributions/4606382/}{ACAT 20 --- AI Decoded: Towards Sustainable, Diverse, Performant and Effective Scientific Computing} & \href{https://eidoom.gitlab.io/acat2021/}{Presented}~\cite{Moodie:2022flt} \\
    & & \emoji{flag-england} & Durham & PhD student seminars & \href{https://eidoom.gitlab.io/acat2021/}{Presented} \\
    & & \emoji{flag-italy} & Turin [V] & QCD journal club & \href{https://eidoom.gitlab.io/acat2021/}{Presented} \\
    & Aug & \emoji{flag-denmark} & Copenhagen [V] & \href{https://indico.nbi.ku.dk/event/1321}{Amplitudes 13} & \\
    & May & \emoji{flag-united-states} & Tallahassee [V] & \href{https://indico.cern.ch/event/958085/}{Joint 15th International Symposium on Radiative Corrections (RADCOR) and the 19th Workshop on Radiative Corrections for the LHC and Future Colliders (LoopFest)} & \\
    & Jul & \emoji{flag-england} & Durham [V] & \href{https://conference.ippp.dur.ac.uk/event/1027}{Young Experimentalists and Theorists Institute (YETI) 2021 --- The Future of Lepton Colliders} & \\
    & Jan & \emoji{flag-france} & Paris [V] & \href{https://indico.desy.de/event/28075}{SAGEX Mathematica and Maple School} & \href{https://gitlab.com/eidoom/project-dkmmr}{Group project} \\
    2020 & Dec & \emoji{flag-england} & Durham [V] & \href{https://conference.ippp.dur.ac.uk/event/937}{YTF 2020} (committee member) & \href{https://eidoom.gitlab.io/ytf20}{Presented} \\
    & Sep & \emoji{flag-european-union} & CERN/Copenhagen/Mainz [V] & \href{https://indico.cern.ch/event/927781}{Elliptics and Beyond} & \\
    & May & \emoji{flag-england} & Durham [V] & \href{https://conference.ippp.dur.ac.uk/event/906}{Remote Conference on New Concepts in Particle Theory (RECONNECT)} & \\
    & & \emoji{flag-united-states} & Michigan/Brown [V] & \href{https://indico.cern.ch/event/908370}{Amplitudes 12} & \\
    & Feb & \emoji{flag-scotland} & Edinburgh & \href{https://conference.ippp.dur.ac.uk/event/844/}{Higgs-Maxwell Workshop 2021} & \\
    & Jan & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/830}{YETI 2020 --- Light new physics} & \\
    2019 & Dec & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/825}{YTF 12} (committee member) & \\
    & & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/781/}{ATM 2019} & \\
    & Sep & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/810}{Pushing the Boundaries---SM and Beyond at the LHC} & \\
    & Aug & \emoji{flag-scotland} & Glasgow & \href{https://sites.google.com/view/busstepp2019/home}{BUSSTEPP summer school} & \href{https://eidoom.gitlab.io/busstepp-slides/body.pdf}{Presented} \\
    & Jun & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/801}{Scattering Amplitudes and Finite Field Arithmetic} & \\
    & Feb & \emoji{flag-scotland} & Edinburgh & \href{https://conference.ippp.dur.ac.uk/event/777/}{Higgs-Maxwell Workshop 2020} & \\
    & Jan & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/723}{YETI 2019 --- To Infinity and Beyond: New Techniques for New Physics} & \\
    2018 & Dec & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/748}{YTF 11} & \\
    & & \emoji{flag-england} & Durham & \href{https://conference.ippp.dur.ac.uk/event/727/}{ATM 2018} & \\
    2017 & Dec & \emoji{flag-england} & Cambridge & Part III Maths Michaelmas Seminar Series & Presented \\
\end{tabularx}\\

\pagebreak
\section{Publications}
\nocite{*}
\hspace{12ex}
\begin{minipage}{154mm}
    \printbibliography[heading=none]
\end{minipage}\\

\end{document}
